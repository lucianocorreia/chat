﻿using Chat.DATA.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.DATA.SERVICE.Repositories
{
    public class UserRepository : RepositoryBase<User>
    {
        public User GetByNickName(string nickname)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                return session.Query<User>().FirstOrDefault(u => u.Nickname.ToUpper() == nickname.ToUpper());
            }
        }
    }
}
