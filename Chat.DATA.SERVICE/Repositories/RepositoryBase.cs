﻿using Chat.DATA.Entities;
using Chat.DATA.SERVICE.Helpers;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.DATA.SERVICE.Repositories
{
    public class RepositoryBase<TEntity> : IRepository<TEntity> where TEntity : IEntity
    {

        protected readonly ISessionFactory _sessionFactory = FluentNHibernateHelper.BuildSessionFactory();

        public void Create(TEntity entity)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                session.Save(entity);

            }
        }

        public void Delete(int id)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                session.Delete(session.Load<TEntity>(id));
            }
        }

        public IQueryable<TEntity> GetAll()
        {
            using (var session = _sessionFactory.OpenSession())
            {
                return session.Query<TEntity>();
            }
        }

        public TEntity GetById(int id)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                return session.Get<TEntity>(id);
            }
        }

        public void Update(TEntity entity)
        {
            using (var session = _sessionFactory.OpenSession())
            {
                session.Update(entity);
            }
        }
    }
}
