﻿using Chat.DATA.Entities;
using Chat.DATA.SERVICE.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.DATA.SERVICE.Service
{
    public class ChatService : IDisposable
    {
        private readonly UserRepository _userRepo = new UserRepository();
        private readonly MessageRepository _msgRepo = new MessageRepository();

        public void SendMessage(string name, string message)
        {
            var user = CreateOrGetUser(name);
            Message msg = new Message() { SenderNickname = user.Nickname, Content = message, Sended = DateTime.Now };
            _msgRepo.Create(msg);
        }

        private User CreateOrGetUser(string nickname)
        {
            var user = _userRepo.GetByNickName(nickname);
            if (user == null)
            {
                user = new User() { Nickname = nickname };
                _userRepo.Create(user);
            }
            return user;
        }

        #region IDisposable Support
        private bool disposedValue = false; // Para detectar chamadas redundantes

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: descartar estado gerenciado (objetos gerenciados).
                }

                // TODO: liberar recursos não gerenciados (objetos não gerenciados) e substituir um finalizador abaixo.
                // TODO: definir campos grandes como nulos.

                disposedValue = true;
            }
        }

        // TODO: substituir um finalizador somente se Dispose(bool disposing) acima tiver o código para liberar recursos não gerenciados.
        // ~ChatService() {
        //   // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
        //   Dispose(false);
        // }

        // Código adicionado para implementar corretamente o padrão descartável.
        public void Dispose()
        {
            // Não altere este código. Coloque o código de limpeza em Dispose(bool disposing) acima.
            Dispose(true);
            // TODO: remover marca de comentário da linha a seguir se o finalizador for substituído acima.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
