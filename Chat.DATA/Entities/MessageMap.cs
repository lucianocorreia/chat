﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.DATA.Entities
{
    public class MessageMap : ClassMap<Message>
    {
        public MessageMap()
        {
            Id(m => m.Id);
            Map(m => m.SenderNickname);
            Map(m => m.Content);
            Map(m => m.Sended);
        }
    }
}
