﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.DATA.Entities
{
    public class Message : IEntity
    {
        public virtual int Id { get; set; }
        public virtual string SenderNickname { get; set; }
        public virtual string ReceiverNickname { get; set; }
        public virtual string Content { get; set; }
        public virtual DateTime Sended { get; set; }
    }
}
