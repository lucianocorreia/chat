﻿using Chat.DATA.Entities;
using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chat.DATA.Entities
{
    public class UserMap : ClassMap<User>
    {
        public UserMap()
        {
            Id(m => m.Id);
            Map(m => m.Nickname);
        }
    }
}
