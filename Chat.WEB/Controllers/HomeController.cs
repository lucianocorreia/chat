﻿using Chat.DATA.Entities;
using Chat.DATA.SERVICE.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Chat.WEB.Controllers
{
    public class HomeController : Controller
    {

        private UserRepository _userRepo = new UserRepository();


        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}