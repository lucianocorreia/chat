﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Chat.WEB.Startup))]
namespace Chat.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
            ConfigureAuth(app);
        }
    }
}
