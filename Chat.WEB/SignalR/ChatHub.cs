﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Chat.DATA.SERVICE.Service;
using Microsoft.AspNet.SignalR;

namespace Chat.WEB.SignalR
{
    public class ChatHub : Hub
    {
        public void send(string name, string message)
        {
            using (ChatService chatService = new ChatService())
            {
                chatService.SendMessage(name, message);

            }
            Clients.All.addMessageToPage(name, message);
        }

    }
}